﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 剧情编辑器3._0
{
    [Serializable]
    public class Scene
    {
        public string name;
        public List<Dialog> dialogs = new List<Dialog>();

        public override string ToString()
        {
            string str = "<scene name=\"" + name + "\">";
            for (int i = 0; i < dialogs.Count; i++)
            {
                Dialog d = dialogs[i];
                str += "\t" + d.ToString() + "\n";
            }
            str += "\t</scene>";
            return str;
        }
    }
}
