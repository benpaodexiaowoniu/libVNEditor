﻿
namespace 剧情编辑器3._0
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip_plottree = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新建ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.剧情ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RenamePlot = new System.Windows.Forms.ToolStripMenuItem();
            this.AddScene = new System.Windows.Forms.ToolStripMenuItem();
            this.RenameScene = new System.Windows.Forms.ToolStripMenuItem();
            this.AddDialog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.MoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.InsertScene = new System.Windows.Forms.ToolStripMenuItem();
            this.视图ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip_polttree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextRenamePlot = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextAddScene = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextRenameScene = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextAddDialog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.ContextMoveUp = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMoveDown = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.ContextInsert = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage_editarea = new System.Windows.Forms.TabPage();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip_plottree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.contextMenuStrip_polttree.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip_plottree
            // 
            this.menuStrip_plottree.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip_plottree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.剧情ToolStripMenuItem,
            this.视图ToolStripMenuItem});
            this.menuStrip_plottree.Location = new System.Drawing.Point(0, 0);
            this.menuStrip_plottree.Name = "menuStrip_plottree";
            this.menuStrip_plottree.Size = new System.Drawing.Size(990, 28);
            this.menuStrip_plottree.TabIndex = 0;
            this.menuStrip_plottree.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新建ToolStripMenuItem,
            this.打开ToolStripMenuItem,
            this.保存ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.关闭ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.文件ToolStripMenuItem.Text = "&F.文件";
            // 
            // 新建ToolStripMenuItem
            // 
            this.新建ToolStripMenuItem.Name = "新建ToolStripMenuItem";
            this.新建ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.新建ToolStripMenuItem.Text = "新建";
            this.新建ToolStripMenuItem.Click += new System.EventHandler(this.CreateNewPlotClick);
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.打开ToolStripMenuItem.Text = "打开";
            this.打开ToolStripMenuItem.Click += new System.EventHandler(this.OpenFileClick);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.SaveFileClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(221, 6);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.关闭ToolStripMenuItem.Text = "关闭";
            // 
            // 剧情ToolStripMenuItem
            // 
            this.剧情ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RenamePlot,
            this.AddScene,
            this.RenameScene,
            this.AddDialog,
            this.toolStripMenuItem2,
            this.MoveUp,
            this.MoveDown,
            this.Delete,
            this.toolStripMenuItem4,
            this.InsertScene});
            this.剧情ToolStripMenuItem.Name = "剧情ToolStripMenuItem";
            this.剧情ToolStripMenuItem.Size = new System.Drawing.Size(66, 24);
            this.剧情ToolStripMenuItem.Text = "&P.剧情";
            // 
            // RenamePlot
            // 
            this.RenamePlot.Name = "RenamePlot";
            this.RenamePlot.Size = new System.Drawing.Size(167, 26);
            this.RenamePlot.Text = "重命名剧情";
            this.RenamePlot.Click += new System.EventHandler(this.RenamePlotClick);
            // 
            // AddScene
            // 
            this.AddScene.Name = "AddScene";
            this.AddScene.Size = new System.Drawing.Size(167, 26);
            this.AddScene.Text = "添加场景";
            this.AddScene.Click += new System.EventHandler(this.AddSceneClick);
            // 
            // RenameScene
            // 
            this.RenameScene.Name = "RenameScene";
            this.RenameScene.Size = new System.Drawing.Size(167, 26);
            this.RenameScene.Text = "重命名场景";
            this.RenameScene.Click += new System.EventHandler(this.RenameSceneClick);
            // 
            // AddDialog
            // 
            this.AddDialog.Name = "AddDialog";
            this.AddDialog.Size = new System.Drawing.Size(167, 26);
            this.AddDialog.Text = "添加对话";
            this.AddDialog.Click += new System.EventHandler(this.AddDialogClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(164, 6);
            // 
            // MoveUp
            // 
            this.MoveUp.Name = "MoveUp";
            this.MoveUp.Size = new System.Drawing.Size(167, 26);
            this.MoveUp.Text = "上移";
            this.MoveUp.Click += new System.EventHandler(this.MoveUpClick);
            // 
            // MoveDown
            // 
            this.MoveDown.Name = "MoveDown";
            this.MoveDown.Size = new System.Drawing.Size(167, 26);
            this.MoveDown.Text = "下移";
            this.MoveDown.Click += new System.EventHandler(this.MoveDownClick);
            // 
            // Delete
            // 
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(167, 26);
            this.Delete.Text = "删除";
            this.Delete.Click += new System.EventHandler(this.DeleteClick);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(164, 6);
            // 
            // InsertScene
            // 
            this.InsertScene.Name = "InsertScene";
            this.InsertScene.Size = new System.Drawing.Size(167, 26);
            this.InsertScene.Text = "插入";
            this.InsertScene.Click += new System.EventHandler(this.InsertClick);
            // 
            // 视图ToolStripMenuItem
            // 
            this.视图ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.刷新ToolStripMenuItem});
            this.视图ToolStripMenuItem.Name = "视图ToolStripMenuItem";
            this.视图ToolStripMenuItem.Size = new System.Drawing.Size(67, 24);
            this.视图ToolStripMenuItem.Text = "&V.视图";
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.RefreshMainFormClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer1.Size = new System.Drawing.Size(990, 575);
            this.splitContainer1.SplitterDistance = 264;
            this.splitContainer1.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(264, 575);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.treeView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(256, 546);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "剧情树";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuStrip_polttree;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(3, 3);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(250, 540);
            this.treeView1.TabIndex = 0;
            this.treeView1.DoubleClick += new System.EventHandler(this.treeView1_DoubleClick);
            // 
            // contextMenuStrip_polttree
            // 
            this.contextMenuStrip_polttree.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip_polttree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextRenamePlot,
            this.ContextAddScene,
            this.ContextRenameScene,
            this.ContextAddDialog,
            this.toolStripMenuItem3,
            this.ContextMoveUp,
            this.ContextMoveDown,
            this.ContextDelete,
            this.toolStripMenuItem5,
            this.ContextInsert});
            this.contextMenuStrip_polttree.Name = "contextMenuStrip_polttree";
            this.contextMenuStrip_polttree.Size = new System.Drawing.Size(154, 208);
            // 
            // ContextRenamePlot
            // 
            this.ContextRenamePlot.Name = "ContextRenamePlot";
            this.ContextRenamePlot.Size = new System.Drawing.Size(153, 24);
            this.ContextRenamePlot.Text = "重命名剧情";
            this.ContextRenamePlot.Click += new System.EventHandler(this.ContextRenamePlotClick);
            // 
            // ContextAddScene
            // 
            this.ContextAddScene.Name = "ContextAddScene";
            this.ContextAddScene.Size = new System.Drawing.Size(153, 24);
            this.ContextAddScene.Text = "添加场景";
            this.ContextAddScene.Click += new System.EventHandler(this.ContextAddSceneClick);
            // 
            // ContextRenameScene
            // 
            this.ContextRenameScene.Name = "ContextRenameScene";
            this.ContextRenameScene.Size = new System.Drawing.Size(153, 24);
            this.ContextRenameScene.Text = "重命名场景";
            this.ContextRenameScene.Click += new System.EventHandler(this.ContextRenameSceneClick);
            // 
            // ContextAddDialog
            // 
            this.ContextAddDialog.Name = "ContextAddDialog";
            this.ContextAddDialog.Size = new System.Drawing.Size(153, 24);
            this.ContextAddDialog.Text = "添加对话";
            this.ContextAddDialog.Click += new System.EventHandler(this.ContextAddDialogClick);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(150, 6);
            // 
            // ContextMoveUp
            // 
            this.ContextMoveUp.Name = "ContextMoveUp";
            this.ContextMoveUp.Size = new System.Drawing.Size(153, 24);
            this.ContextMoveUp.Text = "上移";
            this.ContextMoveUp.Click += new System.EventHandler(this.ContextMoveUpClick);
            // 
            // ContextMoveDown
            // 
            this.ContextMoveDown.Name = "ContextMoveDown";
            this.ContextMoveDown.Size = new System.Drawing.Size(153, 24);
            this.ContextMoveDown.Text = "下移";
            this.ContextMoveDown.Click += new System.EventHandler(this.ContextMoveDownClick);
            // 
            // ContextDelete
            // 
            this.ContextDelete.Name = "ContextDelete";
            this.ContextDelete.Size = new System.Drawing.Size(153, 24);
            this.ContextDelete.Text = "删除";
            this.ContextDelete.Click += new System.EventHandler(this.ContextDeleteClick);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(150, 6);
            // 
            // ContextInsert
            // 
            this.ContextInsert.Name = "ContextInsert";
            this.ContextInsert.Size = new System.Drawing.Size(153, 24);
            this.ContextInsert.Text = "插入";
            this.ContextInsert.Click += new System.EventHandler(this.ContextInsert_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage_editarea);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(722, 575);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage_editarea
            // 
            this.tabPage_editarea.Location = new System.Drawing.Point(4, 25);
            this.tabPage_editarea.Name = "tabPage_editarea";
            this.tabPage_editarea.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_editarea.Size = new System.Drawing.Size(714, 546);
            this.tabPage_editarea.TabIndex = 0;
            this.tabPage_editarea.Text = "编辑区";
            this.tabPage_editarea.UseVisualStyleBackColor = true;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "任何文件(*.*)|*.*|*.vnp(libVN剧情文件)|*.vnp";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "任何文件(*.*)|*.*|*.vnp(libVN剧情文件)|*.vnp";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 603);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip_plottree);
            this.Name = "MainForm";
            this.Text = "剧情编辑器";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip_plottree.ResumeLayout(false);
            this.menuStrip_plottree.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.contextMenuStrip_polttree.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip_plottree;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 新建ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.ToolStripMenuItem 剧情ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AddScene;
        private System.Windows.Forms.ToolStripMenuItem AddDialog;
        private System.Windows.Forms.ToolStripMenuItem RenamePlot;
        private System.Windows.Forms.ToolStripMenuItem RenameScene;
        public System.Windows.Forms.TabPage tabPage_editarea;
        private System.Windows.Forms.ToolStripMenuItem 视图ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_polttree;
        private System.Windows.Forms.ToolStripMenuItem ContextRenamePlot;
        private System.Windows.Forms.ToolStripMenuItem ContextAddScene;
        private System.Windows.Forms.ToolStripMenuItem ContextRenameScene;
        private System.Windows.Forms.ToolStripMenuItem ContextAddDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem MoveUp;
        private System.Windows.Forms.ToolStripMenuItem MoveDown;
        private System.Windows.Forms.ToolStripMenuItem Delete;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem ContextMoveUp;
        private System.Windows.Forms.ToolStripMenuItem ContextMoveDown;
        private System.Windows.Forms.ToolStripMenuItem ContextDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem InsertScene;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem ContextInsert;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

