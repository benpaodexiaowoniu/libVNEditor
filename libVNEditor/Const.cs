﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 剧情编辑器3._0
{
    public static class Const
    {
        public static class Level
        {
            public const int PLOT = 0;
            public const int SCENE = 1;
            public const int DIALOG = 2;
        }
    }
}
