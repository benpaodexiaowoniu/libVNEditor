﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Xml;
using System.IO;



namespace 剧情编辑器3._0
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Init();
        }

        public Plot plot = new Plot();
        void Init()
        {
            plot.name = "新剧情";
            Refresh_treeview();
        }

        private void AddSceneClick(object sender, EventArgs e)
        {
            Scene s = new Scene();
            TreeNode p = treeView1.SelectedNode;
            if(p == null || p.Level != 0)
            {
                MessageBox.Show("只有剧情中可以添加场景.");
                return;
            }
            string scene_name = Interaction.InputBox("请输入场景名");
            s.name = scene_name;
            plot.scenes.Add(s);
            Refresh_treeview();
        }

        public void Refresh_treeview()
        {
            treeView1.Nodes.Clear();
            TreeNode scenes = treeView1.Nodes.Add(plot.name);
            for (int scene_count = 0; scene_count < plot.scenes.Count; scene_count++)
            {
                Scene s = plot.scenes[scene_count];
                TreeNode dialogs = scenes.Nodes.Add((scene_count + 1).ToString() + ": " + s.name);
                for (int dialog_count = 0; dialog_count < s.dialogs.Count; dialog_count++)
                {
                    Dialog d = s.dialogs[dialog_count];
                    dialogs.Nodes.Add((dialog_count + 1).ToString() + ": " + d.speaker);
                }
            }
            treeView1.ExpandAll();
        }

        private void AddDialogClick(object sender, EventArgs e)
        {
            TreeNode scene = treeView1.SelectedNode;
            if(scene.Level != 1)
            {
                MessageBox.Show("只有场景中可以添加对话.");
                return;
            }

            string speaker = Interaction.InputBox("请输入新的对话的说话人名:");
            Dialog d = new Dialog();
            d.speaker = speaker;
            plot.scenes[scene.Index].dialogs.Add(d);
            Refresh_treeview();
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            if (node == null) return;
            switch(node.Level)
            {
                case Const.Level.PLOT:
                    //根
                    break;
                case Const.Level.SCENE:
                    //场景
                    break;
                case Const.Level.DIALOG:
                    //对话
                    int p = node.Parent.Index;
                    Dialog d = plot.scenes[p].dialogs[node.Index];
                    d.TopLevel = false;
                    d.Parent = this.tabPage_editarea;
                    d.Text = plot.name + " -> " + node.Parent.Text +" -> "+ node.Text;
                    d.RefreshForm();
                    d.Show();
                    break;
            }
        }

        private void RenamePlotClick(object sender, EventArgs e)
        {
            string newname = Interaction.InputBox("请输入新的剧情名", "剧情重命名", plot.name);
            if(newname != "")
            {
                plot.name = newname;
                Refresh_treeview();
            }
        }

        private void RenameSceneClick(object sender, EventArgs e)
        {
            TreeNode scene = treeView1.SelectedNode;
            if(scene.Level == Const.Level.SCENE)
            {
                string newname = Interaction.InputBox("请输入新的场景名", "场景重命名", plot.name);
                if (newname != "")
                {
                    plot.scenes[scene.Index].name = newname;
                    Refresh_treeview();
                }
                else
                {
                    MessageBox.Show("新场景名不能为空!");
                }
            }
            else
            {
                MessageBox.Show("请选择一个场景!");
                return;
            }
        }

        private void RefreshMainFormClick(object sender, EventArgs e)
        {
            Refresh_treeview();
        }

        private void ContextRenamePlotClick(object sender, EventArgs e)
        {
            RenamePlotClick(sender, e);
        }

        private void ContextAddSceneClick(object sender, EventArgs e)
        {
            AddSceneClick(sender, e);
        }

        private void ContextRenameSceneClick(object sender, EventArgs e)
        {
            RenameSceneClick(sender, e);
        }

        private void ContextAddDialogClick(object sender, EventArgs e)
        {
            AddDialogClick(sender, e);
        }

        private void MoveUpClick(object sender, EventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            int level = node.Level;
            if (level == Const.Level.PLOT)
            {
                MessageBox.Show("根节点不能被移动!");
                return;
            }
            else if (level == Const.Level.SCENE)
            {
                if (node.Index == 0) return;
                Scene s1, s2;
                s1 = plot.scenes[node.Index];
                s2 = plot.scenes[node.Index - 1];

                plot.scenes[node.Index] = s2;
                plot.scenes[node.Index - 1] = s1;

            }
            else if (level == Const.Level.DIALOG)
            {
                if (node.Index == 0) return;
                Dialog d1, d2;
                d1 = plot.scenes[node.Parent.Index].dialogs[node.Index];
                d2 = plot.scenes[node.Parent.Index].dialogs[node.Index - 1];

                plot.scenes[node.Parent.Index].dialogs[node.Index] = d2;
                plot.scenes[node.Parent.Index].dialogs[node.Index - 1] = d1;
                
            }
            Refresh_treeview();
        }

        private void DeleteClick(object sender, EventArgs e)
        {
            int level = treeView1.SelectedNode.Level;
            if(level == Const.Level.PLOT)
            {
                MessageBox.Show("根节点不能被删除!");
                return;
            }
            else if(level == Const.Level.SCENE)
            {
                //删除场景
                DialogResult result = MessageBox.Show("确定要删除吗？", "删除", MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel) return;
                else if(result == DialogResult.OK)
                {
                    plot.scenes.RemoveAt(treeView1.SelectedNode.Index);
                    Refresh_treeview();
                }
            }
            else if(level == Const.Level.DIALOG)
            {
                DialogResult result = MessageBox.Show("确定要删除吗？", "删除", MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel) return;
                else if (result == DialogResult.OK)
                {
                    int p = treeView1.SelectedNode.Parent.Index;
                    plot.scenes[p].dialogs.RemoveAt(treeView1.SelectedNode.Index);
                    Refresh_treeview();
                }
            }
            Refresh_treeview();
        }

        private void ContextMoveUpClick(object sender, EventArgs e)
        {
            MoveUpClick(sender, e);
        }

        private void ContextMoveDownClick(object sender, EventArgs e)
        {
            MoveDownClick(sender, e);
        }

        private void MoveDownClick(object sender, EventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            int level = node.Level;
            if (level == Const.Level.PLOT)
            {
                MessageBox.Show("根节点不能被移动!");
                return;
            }
            else if (level == Const.Level.SCENE)
            {
                if (node.Index == 0) return;
                Scene s1, s2;
                s1 = plot.scenes[node.Index];
                s2 = plot.scenes[node.Index - 1];

                plot.scenes[node.Index] = s2;
                plot.scenes[node.Index + 1] = s1;

            }
            else if (level == Const.Level.DIALOG)
            {
                if (node.Index == 0) return;
                Dialog d1, d2;
                d1 = plot.scenes[node.Parent.Index].dialogs[node.Index];
                d2 = plot.scenes[node.Parent.Index].dialogs[node.Index - 1];

                plot.scenes[node.Parent.Index].dialogs[node.Index] = d2;
                plot.scenes[node.Parent.Index].dialogs[node.Index + 1] = d1;

            }
            Refresh_treeview();
        }

        private void ContextDeleteClick(object sender, EventArgs e)
        {
            DeleteClick(sender, e);
        }

        private void InsertClick(object sender, EventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            if(node.Level == Const.Level.PLOT)
            {
                return;
            }
            else if(node.Level == Const.Level.SCENE)
            {
                //插入场景
                int idx = node.Index;
                string scene_name = Interaction.InputBox("请输入新场景名:");
                Scene s = new Scene
                {
                    name = scene_name
                };
                plot.scenes.Insert(idx, s);
            }
            else if(node.Level == Const.Level.DIALOG)
            {
                //插入对话
                int idx_scene = node.Parent.Index;
                int idx_dialog = node.Index;
                string speaker_name = Interaction.InputBox("请输入新说话人名:");
                Dialog d = new Dialog()
                {
                    speaker = speaker_name
                };
                plot.scenes[idx_scene].dialogs.Insert(idx_dialog, d);
            }
            Refresh_treeview();
        }

        private void ContextInsert_Click(object sender, EventArgs e)
        {
            InsertClick(sender, e);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void SaveFileClick(object sender, EventArgs e)
        {
            string path;
            DialogResult result = saveFileDialog.ShowDialog();
            if (result != DialogResult.OK) return;
            path = saveFileDialog.FileName;
            File.WriteAllText(path, plot.ToString());
        }

        private void OpenFileClick(object sender, EventArgs e)
        {
            string path;
            DialogResult result = openFileDialog.ShowDialog();
            if (result != DialogResult.OK) return;
            path = openFileDialog.FileName;
            string code = File.ReadAllText(path);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(code);
            XmlNode root = xml.SelectSingleNode("plot");
            XmlNodeList scenes = root.ChildNodes;
            Plot plot_read = new Plot();
            plot_read.name = ((XmlElement)root).GetAttribute("name");
            foreach(XmlNode scene in scenes)
            {
                XmlElement xe_scene = (XmlElement)scene;
                string scene_name = xe_scene.GetAttribute("name");
                Scene s = new Scene();
                s.name = scene_name;
                XmlNodeList dialogs = scene.ChildNodes;
                foreach(XmlNode dialog in dialogs)
                {
                    XmlElement xe_dialog = (XmlElement)dialog;
                    string _speaker = xe_dialog.GetAttribute("speaker");
                    string _value = xe_dialog.GetAttribute("value");
                    XmlNodeList nodes = dialog.ChildNodes;
                    Dialog dlg = new Dialog
                    {
                        speaker = _speaker,
                        value = _value
                    };
                    foreach(XmlNode node in nodes)
                    {
                        XmlElement xe_node = (XmlElement)node;
                        string type = xe_node.Name;
                        if(type == "actions")
                        {
                            XmlNodeList actions = node.ChildNodes;
                            foreach(XmlNode action in actions)
                            {
                                XmlElement xe_action = (XmlElement)action;
                                string cmd = xe_action.GetAttribute("command");
                                string cmdcode = xe_action.Value;
                                Action act = new Action
                                {
                                    command = cmd,
                                    code = cmdcode
                                };
                                dlg.actions.Add(act);
                            }
                        }
                        else if(type == "options")
                        {
                            XmlNodeList options = node.ChildNodes;
                            foreach(XmlNode option in options)
                            {
                                XmlElement xe_option = (XmlElement)option;
                                string _title = xe_option.GetAttribute("title");
                                Option opt = new Option
                                {
                                    title = _title
                                };
                                XmlNodeList actions = option.ChildNodes;
                                foreach(XmlNode action in actions)
                                {
                                    XmlElement xe_action = (XmlElement)action;
                                    string cmd = xe_action.GetAttribute("command");
                                    string cmdcode = xe_action.GetAttribute("code");
                                    Action act = new Action
                                    {
                                        command = cmd,
                                        code = cmdcode
                                    };
                                    opt.actions.Add(act);
                                }
                                dlg.options.Add(opt);
                            }
                        }
                    }
                    s.dialogs.Add(dlg);
                }
                plot_read.scenes.Add(s);
            }
            plot = plot_read;
            Refresh_treeview();
        }

        private void CreateNewPlotClick(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("确定要用一个空的工程覆盖当期工程吗？", "新建", MessageBoxButtons.OKCancel);
            if (result == DialogResult.Cancel) return;
            plot = new Plot();
            plot.name = "新剧情";
        }
    }
}
