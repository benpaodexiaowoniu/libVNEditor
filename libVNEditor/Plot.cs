﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 剧情编辑器3._0
{
    [Serializable]
    public class Plot
    {
        public string name;
        public List<Scene> scenes = new List<Scene>();

        public override string ToString()
        {
            string str = "<plot name=\"" + name + "\">\n";
            for (int i = 0; i < scenes.Count; i++)
            {
                Scene s = scenes[i];
                str += "\t" + s.ToString() + "\n";
            }
            str += "\t</plot>";
            return str;
        }
    }
}
