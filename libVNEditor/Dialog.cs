﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 剧情编辑器3._0
{
    [Serializable]
    public class Dialog : Form
    {
        public string speaker;
        public string value;
        public List<Action> actions = new List<Action>();
        public List<Option> options = new List<Option>();
        private TableLayoutPanel tableLayoutPanel1;
        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox textBox_speaker;
        private TextBox textBox_value;
        private ListView listView_options;
        private Label label4;
        private ListView listView_actions;
        private ContextMenuStrip contextMenuStrip_action;
        private IContainer components;
        private ToolStripMenuItem AddAction;
        private ToolStripMenuItem DeleteAction;
        private ToolStripMenuItem MoveUpAction;
        private ToolStripMenuItem MoveDownAction;
        private ContextMenuStrip contextMenuStrip_options;
        private ToolStripMenuItem AddOption;
        private ToolStripMenuItem DeleteOption;
        private ToolStripMenuItem MoveUpOption;
        private ToolStripMenuItem MoveDownOption;

        public Dialog()
        {
            InitializeComponent();
        }

        public override string ToString()
        {
            string str = "<dialog speaker=\"" + speaker + "\" value=\"" + value + "\">";
            str += "<actions>";    
            for (int i = 0; i < actions.Count; i++)
            {
                Action a = actions[i];
                str += "\t" + a.ToString() + "\n";
            }
            str += "</actions>\n<options>";
            for (int i = 0; i < options.Count; i++)
            {
                Option opt = options[i];
                str += "\t" + opt.ToString() + "\n";
            }
            
            str += "\t</options></dialog>";

            return str;
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.listView_actions = new System.Windows.Forms.ListView();
            this.contextMenuStrip_action = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddAction = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveUpAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveDownAction = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_speaker = new System.Windows.Forms.TextBox();
            this.textBox_value = new System.Windows.Forms.TextBox();
            this.listView_options = new System.Windows.Forms.ListView();
            this.contextMenuStrip_options = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddOption = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteOption = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveUpOption = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveDownOption = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.contextMenuStrip_action.SuspendLayout();
            this.contextMenuStrip_options.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.listView_actions, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox_speaker, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_value, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.listView_options, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(487, 279);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // listView_actions
            // 
            this.listView_actions.ContextMenuStrip = this.contextMenuStrip_action;
            this.listView_actions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_actions.Location = new System.Drawing.Point(63, 209);
            this.listView_actions.Name = "listView_actions";
            this.listView_actions.Size = new System.Drawing.Size(421, 67);
            this.listView_actions.TabIndex = 7;
            this.listView_actions.UseCompatibleStateImageBehavior = false;
            this.listView_actions.View = System.Windows.Forms.View.List;
            this.listView_actions.DoubleClick += new System.EventHandler(this.listView_actions_DoubleClick);
            // 
            // contextMenuStrip_action
            // 
            this.contextMenuStrip_action.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip_action.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddAction,
            this.DeleteAction,
            this.MoveUpAction,
            this.MoveDownAction});
            this.contextMenuStrip_action.Name = "contextMenuStrip_action";
            this.contextMenuStrip_action.Size = new System.Drawing.Size(139, 100);
            // 
            // AddAction
            // 
            this.AddAction.Name = "AddAction";
            this.AddAction.Size = new System.Drawing.Size(138, 24);
            this.AddAction.Text = "添加动作";
            this.AddAction.Click += new System.EventHandler(this.AddActionClick);
            // 
            // DeleteAction
            // 
            this.DeleteAction.Name = "DeleteAction";
            this.DeleteAction.Size = new System.Drawing.Size(138, 24);
            this.DeleteAction.Text = "删除动作";
            this.DeleteAction.Click += new System.EventHandler(this.DeleteActionClick);
            // 
            // MoveUpAction
            // 
            this.MoveUpAction.Name = "MoveUpAction";
            this.MoveUpAction.Size = new System.Drawing.Size(138, 24);
            this.MoveUpAction.Text = "上移动作";
            this.MoveUpAction.Click += new System.EventHandler(this.MoveUpActionClick);
            // 
            // MoveDownAction
            // 
            this.MoveDownAction.Name = "MoveDownAction";
            this.MoveDownAction.Size = new System.Drawing.Size(138, 24);
            this.MoveDownAction.Text = "下移动作";
            this.MoveDownAction.Click += new System.EventHandler(this.MoveDownActionClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "说话人";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "内容";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "选项";
            // 
            // textBox_speaker
            // 
            this.textBox_speaker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_speaker.Location = new System.Drawing.Point(63, 7);
            this.textBox_speaker.Name = "textBox_speaker";
            this.textBox_speaker.Size = new System.Drawing.Size(421, 25);
            this.textBox_speaker.TabIndex = 3;
            this.textBox_speaker.TextChanged += new System.EventHandler(this.textBox_speaker_TextChanged);
            this.textBox_speaker.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SpeakerChanged);
            // 
            // textBox_value
            // 
            this.textBox_value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_value.Location = new System.Drawing.Point(63, 43);
            this.textBox_value.Multiline = true;
            this.textBox_value.Name = "textBox_value";
            this.textBox_value.Size = new System.Drawing.Size(421, 77);
            this.textBox_value.TabIndex = 4;
            this.textBox_value.TextChanged += new System.EventHandler(this.textBox_value_TextChanged);
            // 
            // listView_options
            // 
            this.listView_options.ContextMenuStrip = this.contextMenuStrip_options;
            this.listView_options.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_options.Location = new System.Drawing.Point(63, 126);
            this.listView_options.Name = "listView_options";
            this.listView_options.Size = new System.Drawing.Size(421, 77);
            this.listView_options.TabIndex = 5;
            this.listView_options.UseCompatibleStateImageBehavior = false;
            this.listView_options.View = System.Windows.Forms.View.List;
            this.listView_options.DoubleClick += new System.EventHandler(this.listView_options_DoubleClick);
            // 
            // contextMenuStrip_options
            // 
            this.contextMenuStrip_options.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip_options.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddOption,
            this.DeleteOption,
            this.MoveUpOption,
            this.MoveDownOption});
            this.contextMenuStrip_options.Name = "contextMenuStrip_action";
            this.contextMenuStrip_options.Size = new System.Drawing.Size(139, 100);
            // 
            // AddOption
            // 
            this.AddOption.Name = "AddOption";
            this.AddOption.Size = new System.Drawing.Size(138, 24);
            this.AddOption.Text = "添加选项";
            this.AddOption.Click += new System.EventHandler(this.AddOptionClick);
            // 
            // DeleteOption
            // 
            this.DeleteOption.Name = "DeleteOption";
            this.DeleteOption.Size = new System.Drawing.Size(138, 24);
            this.DeleteOption.Text = "删除选项";
            this.DeleteOption.Click += new System.EventHandler(this.DeleteOptionClick);
            // 
            // MoveUpOption
            // 
            this.MoveUpOption.Name = "MoveUpOption";
            this.MoveUpOption.Size = new System.Drawing.Size(138, 24);
            this.MoveUpOption.Text = "上移选项";
            this.MoveUpOption.Click += new System.EventHandler(this.MoveUpOptionClick);
            // 
            // MoveDownOption
            // 
            this.MoveDownOption.Name = "MoveDownOption";
            this.MoveDownOption.Size = new System.Drawing.Size(138, 24);
            this.MoveDownOption.Text = "下移选项";
            this.MoveDownOption.Click += new System.EventHandler(this.MoveDownOptionClick);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "动作";
            // 
            // Dialog
            // 
            this.ClientSize = new System.Drawing.Size(487, 279);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Dialog";
            this.Text = "对话编辑器";
            this.Load += new System.EventHandler(this.Dialog_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.contextMenuStrip_action.ResumeLayout(false);
            this.contextMenuStrip_options.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void textBox_speaker_TextChanged(object sender, EventArgs e)
        {
            this.speaker = textBox_speaker.Text;
            
        }

        private void textBox_value_TextChanged(object sender, EventArgs e)
        {
            this.value = textBox_value.Text;
            
        }

        private void listView_options_DoubleClick(object sender, EventArgs e)
        {
            if(listView_options.SelectedIndices.Count == 0)
            {
                return;
            }
            else
            {
                int idx = listView_options.SelectedIndices[0];
                options[idx].Show();
            }
        }


        /// <summary>
        /// 刷新窗口的两个listView的显示
        /// </summary>
        public void RefreshForm()
        {
            textBox_speaker.Text = speaker;
            textBox_value.Text = value;
            listView_options.Items.Clear();
            listView_actions.Items.Clear();
            foreach (Option opt in options)
            {
                listView_options.Items.Add(opt.title);
            }

            foreach (Action act in actions)
            {
                listView_actions.Items.Add(act.command);
            }
        }

        /// <summary>
        /// 窗口加载完毕后调用这个函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dialog_Load(object sender, EventArgs e)
        {
            RefreshForm();
        }

        /// <summary>
        /// 重写了OnClosing事件
        /// 窗口要关闭的时候，不让它关闭，但把“可视”属性改为false，模拟窗口关闭
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            e.Cancel = true;
            this.Visible = false;
        }

        /// <summary>
        /// 动作列表被双击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView_actions_DoubleClick(object sender, EventArgs e)
        {
            int cur;
            if (listView_actions.SelectedIndices.Count == 0) return;
            cur = listView_actions.SelectedIndices[0];
            actions[cur].Show();
        }

        /// <summary>
        /// 右键菜单中的添加动作被点击的时候的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddActionClick(object sender, EventArgs e)
        {
            string cmd = Interaction.InputBox("请输入动作命令");
            if(cmd == "")
            {
                MessageBox.Show("命令不能为空");
                return;
            }
            Action a = new Action();
            a.command = cmd;
            actions.Add(a);
            RefreshForm();
        }
        /// <summary>
        /// 右键菜单中的删除动作被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteActionClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count != 0)
            {
                int idx = listView_actions.SelectedIndices[0];
                actions.RemoveAt(idx);
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        /// <summary>
        /// 右键菜单中的上移动作被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveUpActionClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count != 0)
            {
                int idx = listView_actions.SelectedIndices[0];
                if (idx == 0 || actions.Count <= 1) return;
                else
                {
                    Action a1, a2;
                    a1 = actions[idx];
                    a2 = actions[idx - 1];
                    actions[idx] = a2;
                    actions[idx - 1] = a1;
                }
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        /// <summary>
        /// 右键菜单中的下移动作被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveDownActionClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count != 0)
            {
                int idx = listView_actions.SelectedIndices[0];
                if (idx == actions.Count - 1 || actions.Count <= 1) return;
                else
                {
                    Action a1, a2;
                    a1 = actions[idx];
                    a2 = actions[idx + 1];
                    actions[idx] = a2;
                    actions[idx + 1] = a1;
                }
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        /// <summary>
        /// 右键菜单中的添加选项被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddOptionClick(object sender, EventArgs e)
        {
            string title = Interaction.InputBox("请输入选项名称");
            if (title == "")
            {
                MessageBox.Show("选项名不能为空");
                return;
            }
            Option option = new Option();
            option.title = title;
            options.Add(option);
            RefreshForm();
        }

        /// <summary>
        /// 右键菜单中的删除选项被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteOptionClick(object sender, EventArgs e)
        {
            if (listView_options.SelectedIndices.Count != 0)
            {
                int idx = listView_options.SelectedIndices[0];
                actions.RemoveAt(idx);
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个选项!");
                return;
            }
        }

        /// <summary>
        /// 右键菜单中的上移选项被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveUpOptionClick(object sender, EventArgs e)
        {
            if (listView_options.SelectedIndices.Count != 0)
            {
                int idx = listView_options.SelectedIndices[0];
                if (idx == 0 || options.Count <= 1) return;
                else
                {
                    Option a1, a2;
                    a1 = options[idx];
                    a2 = options[idx - 1];
                    options[idx] = a2;
                    options[idx - 1] = a1;
                }
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        /// <summary>
        /// 右键菜单中的下移选项被点击时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveDownOptionClick(object sender, EventArgs e)
        {
            if (listView_options.SelectedIndices.Count != 0)
            {
                int idx = listView_options.SelectedIndices[0];
                if (idx == 0 || options.Count <= 1) return;
                else
                {
                    Option a1, a2;
                    a1 = options[idx];
                    a2 = options[idx + 1];
                    options[idx] = a2;
                    options[idx + 1] = a1;
                }
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        private void SpeakerChanged(object sender, KeyPressEventArgs e)
        {
            
        }
    }
}
