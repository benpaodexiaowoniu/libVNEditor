﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 剧情编辑器3._0
{
    [Serializable]
    public class Option : Form
    {
        public string title = "";
        private TableLayoutPanel tableLayoutPanel1;
        private Label label2;
        private Label label1;
        private TextBox textBox_title;
        private ListView listView_actions;
        private ContextMenuStrip contextMenuStrip_action;
        private System.ComponentModel.IContainer components;
        private ToolStripMenuItem AddAction;
        private ToolStripMenuItem DeleteAction;
        private ToolStripMenuItem MoveUpAction;
        private ToolStripMenuItem MoveDownAction;
        public List<Action> actions = new List<Action>();

        public Option()
        {
            InitializeComponent();
        }

        public override string ToString()
        {
            string str = "<option title=\"" + title + "\">";
            for(int i = 0; i < actions.Count; i++)
            {
                Action a = actions[i];
                str += "\t" + a.ToString() + "\n";
            }
            if (actions.Count > 0) str += "\t" + "</option>";
            return str;
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_title = new System.Windows.Forms.TextBox();
            this.listView_actions = new System.Windows.Forms.ListView();
            this.contextMenuStrip_action = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddAction = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveUpAction = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveDownAction = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1.SuspendLayout();
            this.contextMenuStrip_action.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_title, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.listView_actions, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(531, 132);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "动作";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "标题";
            // 
            // textBox_title
            // 
            this.textBox_title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_title.Location = new System.Drawing.Point(103, 5);
            this.textBox_title.Name = "textBox_title";
            this.textBox_title.Size = new System.Drawing.Size(425, 25);
            this.textBox_title.TabIndex = 1;
            this.textBox_title.TextChanged += new System.EventHandler(this.textBox_title_TextChanged);
            // 
            // listView_actions
            // 
            this.listView_actions.ContextMenuStrip = this.contextMenuStrip_action;
            this.listView_actions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_actions.Location = new System.Drawing.Point(103, 38);
            this.listView_actions.Name = "listView_actions";
            this.listView_actions.Size = new System.Drawing.Size(425, 91);
            this.listView_actions.TabIndex = 3;
            this.listView_actions.UseCompatibleStateImageBehavior = false;
            this.listView_actions.View = System.Windows.Forms.View.List;
            this.listView_actions.SelectedIndexChanged += new System.EventHandler(this.listView_actions_SelectedIndexChanged);
            this.listView_actions.DoubleClick += new System.EventHandler(this.listView_actions_DoubleClick);
            // 
            // contextMenuStrip_action
            // 
            this.contextMenuStrip_action.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip_action.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddAction,
            this.DeleteAction,
            this.MoveUpAction,
            this.MoveDownAction});
            this.contextMenuStrip_action.Name = "contextMenuStrip_action";
            this.contextMenuStrip_action.Size = new System.Drawing.Size(211, 128);
            // 
            // AddAction
            // 
            this.AddAction.Name = "AddAction";
            this.AddAction.Size = new System.Drawing.Size(210, 24);
            this.AddAction.Text = "添加动作";
            this.AddAction.Click += new System.EventHandler(this.AddActionClick);
            // 
            // DeleteAction
            // 
            this.DeleteAction.Name = "DeleteAction";
            this.DeleteAction.Size = new System.Drawing.Size(210, 24);
            this.DeleteAction.Text = "删除动作";
            this.DeleteAction.Click += new System.EventHandler(this.DeleteActionClick);
            // 
            // MoveUpAction
            // 
            this.MoveUpAction.Name = "MoveUpAction";
            this.MoveUpAction.Size = new System.Drawing.Size(210, 24);
            this.MoveUpAction.Text = "上移动作";
            this.MoveUpAction.Click += new System.EventHandler(this.MoveUpActionClick);
            // 
            // MoveDownAction
            // 
            this.MoveDownAction.Name = "MoveDownAction";
            this.MoveDownAction.Size = new System.Drawing.Size(210, 24);
            this.MoveDownAction.Text = "下移动作";
            this.MoveDownAction.Click += new System.EventHandler(this.MoveDownActionClick);
            // 
            // Option
            // 
            this.ClientSize = new System.Drawing.Size(531, 132);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Option";
            this.Text = "选项编辑器";
            this.Load += new System.EventHandler(this.Option_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.contextMenuStrip_action.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        public void RefreshForm()
        {
            textBox_title.Text = title;
            listView_actions.Items.Clear();
            foreach(Action act in actions)
            {
                listView_actions.Items.Add(act.command);
            }
        }
        private void AddActionClick(object sender, EventArgs e)
        {
            string cmd = Interaction.InputBox("请输入动作命令");
            if (cmd == "")
            {
                MessageBox.Show("命令不能为空");
                return;
            }
            Action a = new Action();
            a.command = cmd;
            actions.Add(a);
            RefreshForm();
        }

        private void DeleteActionClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count != 0)
            {
                int idx = listView_actions.SelectedIndices[0];
                actions.RemoveAt(idx);
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        private void MoveUpActionClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count != 0)
            {
                int idx = listView_actions.SelectedIndices[0];
                if (idx == 0 || actions.Count <= 1) return;
                else
                {
                    Action a1, a2;
                    a1 = actions[idx];
                    a2 = actions[idx - 1];
                    actions[idx] = a2;
                    actions[idx - 1] = a1;
                }
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        private void MoveDownActionClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count != 0)
            {
                int idx = listView_actions.SelectedIndices[0];
                if (idx == actions.Count - 1 || actions.Count <= 1) return;
                else
                {
                    Action a1, a2;
                    a1 = actions[idx];
                    a2 = actions[idx + 1];
                    actions[idx + 1] = a1;
                    actions[idx] = a2;

                }
                RefreshForm();
            }
            else
            {
                MessageBox.Show("请先选择一个动作!");
                return;
            }
        }

        private void listView_actions_DoubleClick(object sender, EventArgs e)
        {
            if (listView_actions.SelectedIndices.Count == 0) return;
            else
            {
                int cur = listView_actions.SelectedIndices[0];
                actions[cur].Show();
                
            }
        }

        private void Option_Load(object sender, EventArgs e)
        {
            textBox_title.Text = title;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            e.Cancel = true;
            this.Visible = false;
        }

        private void textBox_title_TextChanged(object sender, EventArgs e)
        {
            this.title = textBox_title.Text;
        }

        private void listView_actions_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
        }

        private void listView_actions_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
